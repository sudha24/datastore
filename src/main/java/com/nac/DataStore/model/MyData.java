package com.nac.DataStore.model;

import javax.persistence.*;
//import javax.validation.constraints.NotBlank;
@Entity
@Table(name = "mydata", schema = "public")

public class MyData {

	@Id
    //@GeneratedValue
	//@NotBlank
    private String key;
	private String data;
	
	public String getKey() {
		return key;
	}
	
	public void setKey(String key) {
		this.key = key;
	}
	
	public String getData() {
		return data;
	}
	
	public void setData(String data) {
		this.data = data;
	}
	
}
