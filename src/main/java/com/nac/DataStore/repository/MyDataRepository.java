package com.nac.DataStore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;


import com.nac.DataStore.model.MyData;
@Repository
public interface MyDataRepository extends JpaRepository<MyData, Long>{
	
	@Query("SELECT t From MyData t where t.key= ?1")
	MyData findDataByKey(String mykey);

}
