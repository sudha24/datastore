package com.nac.DataStore.controller;

import com.nac.DataStore.model.MyData;
import com.nac.DataStore.repository.MyDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
//import org.springframework.http.ResponseEntity;
import javax.validation.Valid;
//import java.util.List;
@RestController
public class MyDataController {

	@Autowired
	MyDataRepository repository;
	
	//Store Data
    @CrossOrigin
	@PostMapping("/store")
	public MyData createData(@Valid @RequestBody MyData mydata) {
		return repository.save(mydata);
	}
	
    @CrossOrigin
	@GetMapping("/get/{key}")
	public MyData getData(@PathVariable(value = "key") String key) {
		
		return repository.findDataByKey(key);
	}
}
